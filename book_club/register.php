<?php
require_once 'common.inc.php';

    if(isset($_POST['action']) and $_POST["action"]=="register"){
    processForm();
    }else{
    displayForm(array(),array(),new Member(array()));
    }
    function displayForm($errorMessages,$missingFields,$member){
      displayPageHeader("Sign up for the book club");

      if($errorMessages){
        foreach ($errorMessages as $errorMessage) {
          echo $errorMessage;
        }
      }else{ ?>
        <p>Thanks for choosing to join our book club</p>
        <p>To register, please fill in your details below and click Send Detials.</p>
        <p>Fields marked with an asterisk( * ) are required.</p>

<?php } ?>
      <form action="register.php" method="post" style="margin-bottom:50px;">
        <div style="width:30em;">
          <input type="hidden" name="action" value="register"/>
          <label for="username"<?php validateField("username",$missingFields)?>>Choose a username *</label>
          <input type="text" name="username" id="username" value="<?php echo $member->getValueEncoded("username") ?>">

          <label for="password1"<?php if($missingFields) echo ' class="error"'  ?>>Choose a password *</label>
          <input type="password" name="password1" id="password1" value=""/>

          <label for="password2"<?php if($missingFields) echo ' class="error"'  ?>>Retype password *</label>
          <input type="password" name="password2" id="password2" value=""/>
        </div>
      </form>

<?php
  }


 ?>
