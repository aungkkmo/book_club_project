<?php
  require_once '../common.inc.php';
  session_start();

  if(isset($_POST["action"]) and $_POST["action"]=="login"){
    processForm();
  }else{
    displayForm(array(),array(),new Member(array()));
  }
  function displayForm($errorMessages,$missingFields,$member){
    displayPageHeader("Login to the book club members' area",true);
    if($errorMessages){
      foreach($errorMessages as $errorMessage){
        echo $errorMessage;
      }
    }else{
 ?>
  <p>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
    </p>
  <?php } ?>
    <form action="login.php" method="post" style="margin-bottom:50px;">
      <div style="width:30em;">
        <input type="hidden" name="name" value="login"/>

        <label for="username"<?php validateField("username",$missingFields) ?>>Username</label>
        <input type="text" name="username" id="username" value="<?php echo $member->getValueEncoded("username") ?>">

        <label for="password"<?php if($missingFields) echo ' class="alt"' ?>>Password</label>
        <input type="password" name="password" id="password" value="">
        <div style="clear:both;">
          <input type="submit" name="submitButton" id="submitButton" value="Login"/>
        </div>
      </div>

    </form>
    <?php
     displayPageFooter();
    }
    function processForm(){
      $requiredFields=array("username","password");
      $missingFields=array();
      $errorMessages=array();

      $member=new Member(array(
        "username"=>isset($_POST["username"]) ? preg_replace("/[^ \-\_a-zA-Z0-9]/", "", $_POST["username"]) : "",
        "password"=>isset($_POST["password"]) ? preg_replace("/[^ \-\_a-zA-Z0-9]/", "", $_POST["password"]) : "",
      ));
      foreach($requiredFields as $requiredField){
        if(!member->getValue($requiredField)){
          $missingFields[]=$requiredField;
        }
      }
      if($missingFields){
        $errorMessages[]='<p class="error">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
        ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nis</p>';
      }elseif(!$loggedInMember=$member->authenticate()){
        $errorMessages[]='<p class="error">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
        ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nis</p>';
      }
      if($errorMessages){
        displayForm($errorMessages,$missingFields,$member);
      }else{
        $_SESSION["member"]=$loggedInMember;
        displayThanks();
      }
    }
    function displayThanks(){
      displayPageHeader("Thanks for logging in!", true);

    ?>
    <p>
      Thank you for logging in.Please proceed to the <a href="index.php">members' area</a>
    </p>
    <?php
      displayPageFooter();
  }
   ?>
